$(window).on('load', function() {
    $(".load-icon").fadeOut("slow");
});

var app = (function(){

    var mainScene = $('.main-scene'), scene1 = $('.scene-1'), scene2 = $('.scene-2'), scene3 = $('.scene-3'), scene4 = $('.scene-4'),
        scene5 = $('.scene-5'), scene6 = $('.scene-6'), scene7 = $('.scene-7'),
        scene8 = $('.scene-8'), scene9 = $('.scene-9'), scene10 = $('.scene-10'),
        results = $('.results'), colorButtons = $('.color-buttons'), answerButtons = $('.answer-buttons'),
        btn = $('.btn'), color = $('.color'),  scene = $('.scene'), startSorting = $('.start-sorting'), call = $('.call');

     scene.addClass('quick-hide'); results.addClass('quick-hide'); answerButtons.hide();
    startSorting.hide(); call.hide(); colorButtons.addClass('hide-buttons');

    setTimeout(function(){
         $('video').fadeOut(2000);
     }, 2000);

    $('.agree').on('click', function(){
        $('.rules').fadeOut(1000);
        setTimeout(function(){
            startSorting.fadeIn(1000);
            call.fadeIn(1000);
        },500);
    });

     var ans = [
         {
             color: '',
             answer: '',
             correctColor: 'red',
             correctAnswer: 'd e'
         },
         {
             color: '',
             answer: '',
             correctColor: 'black',
             correctAnswer: 'b'
         },
         {
             start: '',
             color: '',
             answer: '',
             correctColor: 'red',
             correctAnswer: 'd e'
         },
         {
             color: '',
             answer: '',
             correctColor: 'red',
             correctAnswer: 'd'
         },
         {
             start: '',
             color: '',
             answer: '',
             correctColor: 'red',
             correctAnswer: 'd'
         },
         {
             color: '',
             answer: '',
             correctColor: 'yellow',
             correctAnswer: 'a'
         },
         {
             color: '',
             answer: '',
             correctColor: 'red',
             correctAnswer: 'd e'
         },
         {
             color: '',
             answer: '',
             correctColor: 'green',
             correctAnswer: 'a'
         },
         {
             color: '',
             answer: '',
             correctColor: 'yellow',
             correctAnswer: 'a'
         },
         {
             color: '',
             answer: '',
             correctColor: 'yellow',
             correctAnswer: 'a'
         }
     ];

     var talkStart = false;
     var correct = 0;
     var score = 0;

    startSorting.on('click', startTest);
    call.on('click', startTest2);

    $('.see-answers').on('click', seeAnswers);

    $('.scene-1 .color').on('click', scene1step1);
    $('.scene-1 .btn').on('click', scene1step2);

    $('.scene-2 .color').on('click', scene2step1);
    $('.scene-2 .btn').on('click', scene2step2);

    $('.scene-3 .intro-buttons button').on('click', scene3step0);
    $('.scene-3 .color').on('click', scene3step1);
    $('.scene-3 .btn').on('click', scene3step2);

    $('.scene-4 .color').on('click', scene4step1);
    $('.scene-4 .btn').on('click', scene4step2);

    $('.scene-5 .intro-buttons button').on('click', scene5step0);
    $('.scene-5 .color').on('click', scene5step1);
    $('.scene-5 .btn').on('click', scene5step2);

    $('.scene-6 .color').on('click', scene6step1);
    $('.scene-6 .btn').on('click', scene6step2);

    $('.scene-7 .color').on('click', scene7step1);
    $('.scene-7 .btn').on('click', scene7step2);

    $('.scene-8 .color').on('click', scene8step1);
    $('.scene-8 .btn').on('click', scene8step2);

    $('.scene-9 .color').on('click', scene9step1);
    $('.scene-9 .btn').on('click', scene9step2);

    $('.scene-10 .color').on('click', scene10step1);
    $('.scene-10 .btn').on('click', scene10step2);

    function startTest() {
        mainScene.addClass('hide').removeClass('show');
        score --;
        setTimeout(function(){
              scene3.addClass('show').removeClass('hide');
        },2500);
    }
    function startTest2() {
        mainScene.addClass('hide').removeClass('show');
        setTimeout(function(){
            scene8.addClass('show').removeClass('hide');
            colorButtons.addClass('show').removeClass('hide-buttons');
            talkStart = true;
        },2500);
    }

    function checkAnswers(){

        for(var i=0; i < ans.length ; i++){

            if(i == 2 || i == 4){
                if(ans[i].start !== 'b'){
                    score --;
                }
            }

            if(ans[i].color === ans[i].correctColor && (ans[i].correctAnswer.indexOf(ans[i].answer) > -1)){
                correct ++;
                score ++;
                ans[i].solved = true;
            }
        }
        sendAjax();

        $('.correct-answers h1').text('Teisingai atsakėte į '+correct+' iš 10 klausimų');
    }

    function seeAnswers() {

        results.addClass('hide').removeClass('show');

        setTimeout(function(){
            $('.display-results').addClass('show').removeClass('hide');
        },2500);

        for(var i=0; i < ans.length ; i++){
            var j = i+1;
            if(ans[i].color === ans[i].correctColor && (ans[i].correctAnswer.indexOf(ans[i].answer) > -1)){
                $('.person-' + j).css('background-image', 'url(build/images/'+j+'g.png)');
            }else{
                $('.person-' + j).css('background-image', 'url(build/images/'+j+'r.png)');
            }
        }

    }

    function sendAjax(){

        var atsakymai = '';

        atsakymai += 'score: ' +score+ ' || ';

        for(var i=0; i < ans.length ; i++) {

            var j = i + 1;

            if(j == 3 || j == 5){
                atsakymai += j+' - '+ans[i].start+', '+ans[i].color+', '+ans[i].answer+'|| ';
            }else{
                atsakymai += j+' - '+ans[i].color+', '+ans[i].answer+'|| ';
            }

        }

        console.log(atsakymai);

            $.ajax({
                type: "POST",
                url: 'insert.php',
                crossDomain: true,
                dataType: 'json',
                data: {
                    atsakymai: atsakymai
                }
            });

    }

    function scene3step0(){
        ans[2].start = $(this).attr('value');
        $('.intro-buttons button').fadeOut(500);
        colorButtons.addClass('show-buttons').removeClass('hide-buttons');

    }
    function scene3step1(){
        ans[2].color = $(this).attr('value');
        colorButtons.addClass('hide-buttons').removeClass('show-buttons');
        answerButtons.fadeIn(1000);
    }
    function  scene3step2() {
        ans[2].answer = $(this).attr('value');

        scene3.addClass('hide').removeClass('show');
        answerButtons.fadeOut(1000);
        setTimeout(function(){
            $('.intro-buttons button').fadeIn(500);
            scene2.addClass('show').removeClass('hide');
            colorButtons.addClass('show').removeClass('hide-buttons');
        },2500);
    }

    function scene2step1(){
        ans[1].color = $(this).attr('value');
        colorButtons.addClass('hide-buttons').removeClass('show');
        answerButtons.fadeIn(1000);
    }
    function  scene2step2() {
        ans[1].answer = $(this).attr('value');

        scene2.addClass('hide').removeClass('show');
        answerButtons.fadeOut(1000);
        setTimeout(function(){
            scene4.addClass('show').removeClass('hide');
            colorButtons.addClass('show').removeClass('hide-buttons');
        },2500);
    }


    function scene4step1(){
        ans[3].color = $(this).attr('value');
        colorButtons.addClass('hide-buttons').removeClass('show');
        answerButtons.fadeIn(1000);
    }
    function  scene4step2() {
        ans[3].answer = $(this).attr('value');

        scene4.addClass('hide').removeClass('show');
        answerButtons.fadeOut(1000);
        setTimeout(function(){
            scene6.addClass('show').removeClass('hide');
            colorButtons.addClass('show').removeClass('hide-buttons');
        },2500);
    }


    function scene6step1(){
        ans[5].color = $(this).attr('value');
        colorButtons.addClass('hide-buttons').removeClass('show');
        answerButtons.fadeIn(1000);
    }
    function  scene6step2() {
        ans[5].answer = $(this).attr('value');

        scene6.addClass('hide').removeClass('show');
        answerButtons.fadeOut(1000);
        setTimeout(function(){
            scene1.addClass('show').removeClass('hide');
            colorButtons.addClass('show').removeClass('hide-buttons');
        },2500);
    }

    function scene1step1(){
        ans[0].color = $(this).attr('value');
        colorButtons.addClass('hide-buttons').removeClass('show');
        answerButtons.fadeIn(1000);
    }
    function  scene1step2() {
        ans[0].answer = $(this).attr('value');

        scene1.addClass('hide').removeClass('show');
        answerButtons.fadeOut(1000);
        setTimeout(function(){
            scene5.addClass('show').removeClass('hide');
        },2500);
    }

    function scene5step0(){
        ans[4].start = $(this).attr('value');
        $('.intro-buttons button').fadeOut(500);
        colorButtons.addClass('show').removeClass('hide-buttons');

    }
    function scene5step1(){
        ans[4].color = $(this).attr('value');
        colorButtons.addClass('hide-buttons').removeClass('show');
        answerButtons.fadeIn(1000);
    }
    function  scene5step2() {
        ans[4].answer = $(this).attr('value');

        scene5.addClass('hide').removeClass('show');
        answerButtons.fadeOut(1000);
        setTimeout(function(){
            scene9.addClass('show').removeClass('hide');
            colorButtons.addClass('show').removeClass('hide-buttons');
        },2500);
    }

    function scene9step1(){
        ans[8].color = $(this).attr('value');
        colorButtons.addClass('hide-buttons').removeClass('show');
        answerButtons.fadeIn(1000);
    }
    function  scene9step2() {
        ans[8].answer = $(this).attr('value');

        scene9.addClass('hide').removeClass('show');
        answerButtons.fadeOut(1000);
        setTimeout(function(){
            scene10.addClass('show').removeClass('hide');
            colorButtons.addClass('show').removeClass('hide-buttons');
        },2500);
    }


    function scene10step1(){
        ans[9].color = $(this).attr('value');
        colorButtons.addClass('hide-buttons').removeClass('show');
        answerButtons.fadeIn(1000);
    }
    function  scene10step2() {
        ans[9].answer = $(this).attr('value');

        scene10.addClass('hide').removeClass('show');
        answerButtons.fadeOut(1000);
        setTimeout(function(){
            scene7.addClass('show').removeClass('hide');
            colorButtons.addClass('show').removeClass('hide-buttons');
        },2500);
    }

    function scene7step1(){
        ans[6].color = $(this).attr('value');
        colorButtons.addClass('hide-buttons').removeClass('show');
        answerButtons.fadeIn(1000);
    }
    function  scene7step2() {
        ans[6].answer = $(this).attr('value');
        if(talkStart == false) {
            scene7.addClass('hide').removeClass('show');
            answerButtons.fadeOut(1000);
            setTimeout(function(){
                scene8.addClass('show').removeClass('hide');
            },2500);
        }else{
            scene7.addClass('hide').removeClass('show');
            answerButtons.fadeOut(1000);
            setTimeout(function(){
                checkAnswers();
                results.addClass('show').removeClass('hide');
            },2500);
        }
        colorButtons.addClass('show').removeClass('hide-buttons');
    }

    function scene8step1(){
        ans[7].color = $(this).attr('value');
        colorButtons.addClass('hide-buttons').removeClass('show');
        answerButtons.fadeIn(1000);
    }
    function  scene8step2() {
        ans[7].answer = $(this).attr('value');
        if(talkStart == false) {
            scene8.addClass('hide').removeClass('show');
            answerButtons.fadeOut(1000);
            setTimeout(function(){
                 results.addClass('show').removeClass('hide');
                 checkAnswers();
            },2500);
        }else{
            scene8.addClass('hide').removeClass('show');
            answerButtons.fadeOut(1000);
            setTimeout(function(){
                   scene3.addClass('show').removeClass('hide');
            },2500);
        }
    }

    $('.scene-1 .mouth').on('click', function(){
       $('.scene-1 .scene-text .flex p').text('Nereaguoja. Nevykdo paprastų komandų.');
    });
    $('.scene-1 .chest').on('click', function(){
        $('.scene-1 .scene-text .flex p').text('Pavertus ant nugaros kvėpavimas 1/10 s.');
    });
    $('.scene-1 .wrist-1').on('click', function(){
        $('.scene-1 .scene-text .flex p').text('Pulsas – 8/10 s.');
    });

    $('.scene-2 .mouth').on('click', function(){
        $('.scene-2 .scene-text .flex p').text('Nereaguoja, nevykdo paprastų komandų.');
    });
    $('.scene-2 .chest').on('click', function(){
        $('.scene-2 .scene-text .flex p').text('Nekvėpuoja.');
    });
    $('.scene-2 .wrist-1').on('click', function(){
        $('.scene-2 .scene-text .flex p').text('Pulsas – 0/10 s.');
    });

    $('.scene-3 .mouth').on('click', function(){
        $('.scene-3 .scene-text .flex p').text('Komandų nevykdo, reaguoja.');
    });
    $('.scene-3 .chest').on('click', function(){
        $('.scene-3 .scene-text .flex p').text('Kvėpavimas 6/10 s.');
    });
    $('.scene-3 .wrist-1').on('click', function(){
        $('.scene-3 .scene-text .flex p').text('Pulsas – 18/10 s.');
    });


    $('.scene-4 .mouth').on('click', function(){
        $('.scene-4 .scene-text .flex p').text('Nevykdo paprastų komandų.');
    });
    $('.scene-4 .chest').on('click', function(){
        $('.scene-4 .scene-text .flex p').text('Kvėpavimas 6/10 s.');
    });
    $('.scene-4 .wrist-1').on('click', function(){
        $('.scene-4 .scene-text .flex p').text('Pulsas – 14/10 s.');
    });

    $('.scene-5 .mouth').on('click', function(){
        $('.scene-5 .scene-text .flex p').text('Komandų nevykdo, reaguoja.');
    });
    $('.scene-5 .chest').on('click', function(){
        $('.scene-5 .scene-text .flex p').text('Kvėpavimas 5/10 s.');
    });
    $('.scene-5 .wrist-1').on('click', function(){
        $('.scene-5 .scene-text .flex p').text('Pulsas – 22/10 s.');
    });

    $('.scene-6 .mouth').on('click', function(){
        $('.scene-6 .scene-text .flex p').text('Reaguoja į prisilietimą, vykdo paprastas komandas gestų kalba.');
    });
    $('.scene-6 .chest').on('click', function(){
        $('.scene-6 .scene-text .flex p').text('Kvėpavimas 4/10 s.');
    });
    $('.scene-6 .wrist-1').on('click', function(){
        $('.scene-6 .scene-text .flex p').text('Pulsas – 17/10 s.');
    });

    $('.scene-7 .mouth').on('click', function(){
        $('.scene-7 .scene-text .flex p').text('Nevykdo paprastų komandų.');
    });
    $('.scene-7 .chest').on('click', function(){
        $('.scene-7 .scene-text .flex p').text('Nekvėpuoja, atlenkus galvą pradeda intensyviai ir giliai kvėpuoti. Kvėpavimas 6/10 s.');
    });
    $('.scene-7 .wrist-1').on('click', function(){
        $('.scene-7 .scene-text .flex p').text('Pulsas – 24/10 s.');
    });

    $('.scene-8 .mouth').on('click', function(){
        $('.scene-8 .scene-text .flex p').text('Reaguoja, vykdo komandas.');
    });
    $('.scene-8 .chest').on('click', function(){
        $('.scene-8 .scene-text .flex p').text('Kvėpavimas 5/10 s.');
    });
    $('.scene-8 .wrist-1').on('click', function(){
        $('.scene-8 .scene-text .flex p').text('Pulsas – 11/10 s.');
    });

    $('.scene-9 .mouth').on('click', function(){
        $('.scene-9 .scene-text .flex p').text('Reaguoja, vykdo paprastas komandas.');
    });
    $('.scene-9 .chest').on('click', function(){
        $('.scene-9 .scene-text .flex p').text('Kvėpavimas 4/10 s.');
    });
    $('.scene-9 .wrist-1').on('click', function(){
        $('.scene-9 .scene-text .flex p').text('Pulsas – 17/10 s.');
    });

    $('.scene-10 .mouth').on('click', function(){
        $('.scene-10 .scene-text .flex p').text('Skundžiasi stuburo juosmeninės dalies skausmu, nejaučia abiejų kojų.');
    });
    $('.scene-10 .chest').on('click', function(){
        $('.scene-10 .scene-text .flex p').text('Kvėpavimas 5/10 s.');
    });
    $('.scene-10 .wrist-1').on('click', function(){
        $('.scene-10 .scene-text .flex p').text('Pulsas – 17/10 s.');
    });

})();
